import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import pic from "../../Static/Images/p1.jpg";

export const Paragraph = props => {
  const { isLeft } = props;
  return (
    <Row>
      <Col className="sm{6} md={6} lg={6} offset-2">
        <h3 className="text-lg-left mt-5">Custom Web Devlopment</h3>
        <p className="text-lg-left mt-5 font-weight-light">
          Web development is the work involved in developing a website for the
          Internet (World Wide Web) or an intranet (a private network).[1] Web
          development can range from developing a simple single static page of
          plain text to complex web-based internet applications (web apps),{" "}
        </p>
      </Col>
      <Col className="sm{6} md={6} lg={6}">
        <img src={pic} className="img fluid mt-4 "></img>
      </Col>
    </Row>
  );
};

export default Paragraph;
