import React from "react";
import "./Coverstyle.css";
import { Container } from "react-bootstrap";

const BackgroundImagePage = () => {
  return (
    <Container fluid>
      <div className="bg">
        <h1 className="text-center font-weight-bolder">
          <span> Web Design & Devlopment</span>
        </h1>
        <p className="text-center Quotes mt-5">
          “Your website is the center of your digital eco-system, like a brick
          <br></br>
          and mortar location, the experience matters once a customer enters,
          just
          <br></br> as much as the perception they have of you before they walk
          through the door.”
        </p>
      </div>
    </Container>
  );
};

export default BackgroundImagePage;
