import React, { Component } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { MdLocalPhone } from "react-icons/md";
import logo from "../../Static/Images/logo.jpg";
import "./header.css";
export default class Header extends Component {
  render() {
    return (
      <Navbar bg="light">
        <Navbar.Brand href="#home">
          {""}
          <img src={logo} className="logo"></img>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <div className="spacer">
            <Nav className="m-auto  text-lg-light">
              <Nav.Link href="#home" className="font-weight-bold  ">
                <span className="color1"> Work</span>
              </Nav.Link>
              <Nav.Link href="#features" className="font-weight-bold  ">
                <span> SERVICE</span>
              </Nav.Link>
              <Nav.Link href="#pricing" className="font-weight-bold ">
                <span>TOOL & TECH</span>
              </Nav.Link>
              <Nav.Link href="#pricing" className="font-weight-bold">
                <span>ABOUT US</span>
              </Nav.Link>
              <Nav.Link href="#pricing" className="font-weight-bold">
                <span>BLOG</span>
              </Nav.Link>
              <Nav.Link href="#pricing" className="font-weight-bold ">
                <span> CAREER</span>
              </Nav.Link>
              <Nav.Link href="#pricing" className="font-weight-bold ">
                <span> CONTACT</span>
              </Nav.Link>
              <Nav.Link href="#pricing" className="font-weight-bold ">
                <span>
                  <MdLocalPhone />
                </span>
                <span> +1-48-520-9597</span>
              </Nav.Link>
            </Nav>
          </div>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
