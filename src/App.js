import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./Components/Header";
import Cover from "./Components/CoverPic";
import Para from "./Components/para";

function App() {
  return (
    <div className="App">
      <Header />
      <Cover />
      <Para />
    </div>
  );
}

export default App;
